package org.example.application.auth.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.auth.User;
import org.example.application.auth.UserView;
import org.example.application.infrastructure.menu.MenuItem;

@RequiredArgsConstructor
public class LoginMenuItem implements MenuItem {

    private final AuthService authService;
    private final UserView userView;

    @Override
    public String getName() {
        return "Login";
    }

    @Override
    public void run() {
        User user = userView.readUser();
        try {
            authService.login(user);
        } catch (IllegalArgumentException exception) {
            userView.showError(exception.getMessage());
        }
    }

    @Override
    public boolean isVisible() {
        return !authService.isAuth();
    }
}
