package org.example.application.ticket.storage;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.RequiredArgsConstructor;
import org.example.application.ticket.Ticket;
import org.example.application.ticket.TicketStorage;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
@RequiredArgsConstructor
public class JsonFileTicketStorage implements TicketStorage {
    private final String filePath;
    private final ObjectMapper objectMapper;


    @Override
    public Stream<Ticket> getAll() {
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        try {
            return objectMapper.readValue(new File(filePath), new TypeReference<List<Ticket>>() {
            }).stream();
        } catch (IOException e) {
            return Stream.empty();
        }

    }

    @Override
    public void save(Ticket ticket) {
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        List<Ticket> tickets = getAll().collect(Collectors.toList());
        tickets.add(ticket);
        try {
            objectMapper.writeValue(new File(filePath), tickets);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void update(Optional<Ticket> ticket) {
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);

        List<Ticket> tickets = getAll().map(t -> {
            if (t.getId().equals(ticket.get().getId())) {
                return ticket.get();
            } else return t;
        }).collect(Collectors.toList());

        try {
            objectMapper.writeValue(new File(filePath), tickets);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
