package org.example.application.ticket.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.infrastructure.menu.MenuItem;
import org.example.application.ticket.TicketService;
import org.example.application.ticket.TicketView;

@RequiredArgsConstructor

public class ChangeTicketStatusMenuItem implements MenuItem {
    private final AuthService authService;
    private final TicketView ticketView;
    private final TicketService ticketService;

    @Override
    public String getName() {
        return "Change Status";
    }

    @Override
    public void run() {
        String id = ticketView.readTicketId();
        ticketView.searchTicketById(id);
        String status = ticketView.readStatus();
        ticketService.changeStatus(id, status);

    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
