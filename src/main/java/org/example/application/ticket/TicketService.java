package org.example.application.ticket;

import lombok.RequiredArgsConstructor;
import java.util.Optional;
import java.util.stream.Stream;
@RequiredArgsConstructor
public class TicketService {
    private final TicketStorage ticketStorage;
    public void addTicket (Ticket ticket){
        ticketStorage.save(ticket);
    }

    public Stream<Ticket> getAllTickets() {
        return ticketStorage.getAll();
    }
    public Stream<Ticket> filterByCreatedUser(String login) {
        return ticketStorage.getAll().filter(ticket -> ticket.getCreatedUser().equals(login));
    }

    public Stream<Ticket> filterByAssigneeUser (String login) {
        return ticketStorage.getAll().filter(ticket -> ticket.getAssigneeUser().equals(login));
    }
    public Optional<Ticket>findTicketById(String id) {
        return ticketStorage.getAll().filter(ticket -> ticket.getId().equals(id)).findFirst();
    }
    public Stream<Ticket> filterTicketByStatus(String status) {
        return ticketStorage.getAll().filter(ticket -> ticket.getStatus().equals(status));
    }

    public void addComment (String id, String comment) {
        Optional<Ticket> ticket = findTicketById(id);
        ticket.get().setComment(comment);
        ticketStorage.update(ticket);
    }
    public void assignUser(String id, String userLogin){
        Optional<Ticket> ticket = findTicketById(id);
        ticket.get().setAssigneeUser(userLogin);
        ticketStorage.update(ticket);
    }

    public void changeStatus(String id, String status) {
        Optional<Ticket> ticket = findTicketById(id);
        ticket.get().setStatus(status);
        ticketStorage.update(ticket);
    }

}
