package org.example.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.application.auth.AuthService;
import org.example.application.auth.UserStorage;
import org.example.application.auth.UserView;
import org.example.application.auth.menu.LoginMenuItem;
import org.example.application.auth.menu.LogoutMenuItem;
import org.example.application.auth.menu.RegisterMenuItem;
import org.example.application.auth.storage.JsonFileUserStorage;
import org.example.application.infrastructure.menu.ExitMenuItem;
import org.example.application.infrastructure.menu.Menu;
import org.example.application.infrastructure.menu.MenuItem;
import org.example.application.infrastructure.menu.Submenu;
import org.example.application.infrastructure.ui.ViewHelper;
import org.example.application.ticket.TicketService;
import org.example.application.ticket.TicketStorage;
import org.example.application.ticket.TicketView;
import org.example.application.ticket.menu.*;
import org.example.application.ticket.storage.JsonFileTicketStorage;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ViewHelper viewHelper = new ViewHelper(scanner);
        ObjectMapper objectMapper = new ObjectMapper();

        UserView userView = new UserView(viewHelper);
        //UserStorage userStorage = new InMemoryUserStorage();
        UserStorage userStorage = new JsonFileUserStorage("./users.json",objectMapper);
        //UserStorage userStorage = new TextFileUserStorage("./users.txt");
        AuthService authService = new AuthService(userStorage);
        TicketStorage ticketStorage = new JsonFileTicketStorage("./tickets.json", objectMapper);
        TicketService ticketService = new TicketService(ticketStorage);
        TicketView ticketView = new TicketView(viewHelper, authService, ticketService);

        List<MenuItem> manageTicketItems = List.of(
                new AddCommentMenuItem(authService, ticketView, ticketService),
                new ChangeTicketAssigneeMenuItem(authService, ticketView, ticketService),
                new ChangeTicketStatusMenuItem(authService, ticketView, ticketService),
                new ExitMenuItem()
        );



        List<MenuItem> items = List.of(
                new LoginMenuItem(authService, userView),
                new RegisterMenuItem(authService,userView),
                new AddTicketMenuItem(authService, ticketView, ticketService),
                new AllTicketsMenuItem(authService, ticketView),
                new MyAssignedTicketsMenuItem(authService, ticketView),
                new MyCreatedTicketsMenuItem(authService, ticketView),
                new FindTicketsByStatusMenuItem(authService, ticketView),
                new Submenu(authService, "Manage Ticket", new Menu(manageTicketItems, scanner)),

                new LogoutMenuItem(authService),
                new ExitMenuItem()
        );

        Menu menu = new Menu(items, scanner);
        menu.run();
        
    }
}
