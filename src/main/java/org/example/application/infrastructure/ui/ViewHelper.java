package org.example.application.infrastructure.ui;

import lombok.RequiredArgsConstructor;

import java.util.Scanner;

@RequiredArgsConstructor
public class ViewHelper {
    private final Scanner scanner;

    public String readString(String message){
        System.out.print(message);
        return scanner.nextLine();
    }

    public int readInt(String message){
        System.out.print(message);
        int val =  scanner.nextInt();
        scanner.nextLine();
        return val;
    }

    public void showError(String error) {
        System.err.println(error);
    }
}
