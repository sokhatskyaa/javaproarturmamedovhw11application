package org.example.application.infrastructure.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
@RequiredArgsConstructor
public class Submenu implements MenuItem{
    private final AuthService authService;
    private final String name;
    private final Menu menu;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void run() {
        menu.run();
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
